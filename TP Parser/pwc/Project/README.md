```
Michaël PÉRIN,
VERIMAG / Univ. Grenoble-Alpes / INP / Polytech Grenoble,
December 2021
```

# Redesign of JavaCC

A long term project with 3rd year students at Polytech Grenoble Engineering school

Part of the course __"Parsers which Compute"__

## Why redesigning JavaCC?

### JavaCC pitfalls

- Two different syntaxes: one for the lexer, one for the parser.

> The syntax could be the same. The main difference between a lexer and a parser is that
lexer recognize regular langages, while parsers deal with context-free grammar.

- The JavaCC Lexer syntax is a (not very clean) mix of regular expressions, actions, and automata states.

- JavaCC parsers work as a pipe. The lexer reads char stream. The parser reads token produced by the lexer. This cannot be changed and it does not allow each parsing method to use its own parser to analyse the char stream.

### Proposal

- A unique syntax for lexers and parsers : both are parsers. Some are regular and can be efficiently implemented using finite automata.

- Generally speaking, a parser is a stream to stream transformation : `Stream<A> -> Stream<B>`

- A lexer is just a particular case : `Stream<char> -> Stream<Token>`

- Lexers and parsers should cooperate freely as stream transformers, not just is a two phases pipe `(lexer ; parser)`

### Open questions

- The nice features of JavaCC is the possibility to do Java computation inside matching.
Instead of a table of transitions, the JavaCC compiler generates the automaton as Java code using `switch` introducing dead code.
:question:
Is it possible to introduce action in a table representations? while still being able to compute a minimimal representation ?


# Road Plan

## :thumbsup: [Project 2020](2020/) - AEF to DOT

**Available:**
- A [syntax](doc/AEF_SYNTAX.md) for describing Finite Automata as `.aef` file
- A [parser](src/aef/) (in JavaCC) which converts `.aef` to `.dot` format for visualization with graphwiz
- detection of sink states and unreachable states.


## [Project 2021](2021/) - GRM to AEF + DFA engine

- A syntax for describing Regular Grammar as `.grm`file
- A parser (in JavaCC) which converts `.grm` to several automata format (`.aef`, `.dot`, transition table in `.html`)
+ Bonus: A java engine for Deterministic Finite Automata (DFA)
+ Bonus: Two extensions of the GRM syntax


## Project 2022 - REGEXP to FSA


## Project 2023 - GRM+ and Thompson constructions

- Extension of the GRM syntax with regular expression
- Extension of the parser
- Thompson constructions of the finite automaton

+ Bonus: optimized Thompson constructions to reduce epsilon
+ Bonus: Elimination of epsilon-transitions


## Project 2023 - Extension of REGEXP + NFA engine

- Several extensions of the Regular Grammar syntax, eg., ranges `0..9 a..z A..Z`
- A Java engine for Non deterministic Finite Automata (NFA)

+ Bonus: Generation of the execution tree of the NFA

## Project 2024 - Extension to the full set of operators on Automata, including negation

- Extension of the GRM syntax with intersection and negation
- Algorithm on automata: Prodcut, Determinisation and complementary automaton

+ Bonus: optimized version of negation on set of symbols (avoiding complementary)

## Project 2025 - Java actions into the grammar
