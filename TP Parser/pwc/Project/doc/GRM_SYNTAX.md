
```Java
PARSE ::= GRAMMAR . \EOF

GRAMMAR ::= "Grammar" . "(" . IDENT . ")" . "{" . RULES . "}" . RUNS

//IDENT ::= SYMBOLS

RULES ::= (RULE)*     // RULE . (RULES)?

RULE ::=  IDENT . "::=" . ("|")? . PRODUCTIONS

PRODUCTIONS ::= (PRODUCTION)*     //(PRODUCTION . PRODUCTIONS) | $

PRODUCTION ::= STRING . IDENT

STRING ::= QUOTE . (SYMBOL)* . QUOTE

// SYMBOLS ::= SYMBOL . SYMBOLS | $

SYMBOL ::= 
 | a
 ...
 | z
 | A
 ...
 | Z
 | 0
 ...
 | 9
 |_

WORD ::= (SYMBOL)*

RUNS ::= (RUN)*     //(RUN . RUNS) | $ 

RUN ::= IDENT . "(" . WORD . ")" . ";"

// extensions (++) et (+++)

EXPR ::=
  | STRING
  | IDENT
  | CHOICE
  | SEQUENCE

CHOICE ::= ...

SEQUENCE ::= EXPR . ( "." EXPR )*
```
