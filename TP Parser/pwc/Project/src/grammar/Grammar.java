package grammar;

import java.util.ArrayList;

import shared.OPTIONS;
import shared.TRACING;

import export.GRM;

import finite_automata.FSA;
import finite_automata.Run;
import finite_automata.State;
import finite_automata.SharedStates;
import finite_automata.Symbol;
import finite_automata.SharedSymbols;

public class Grammar {

	// SHARING

	private SharedStates _all_states;
	private SharedSymbols _all_symbols;

	public State state(String name) {
		return _all_states.state(name);
	}

	public Symbol symbol(String letter) {
		return _all_symbols.symbol(letter);
	}

	// FIELDS

	private String _name;
	private ArrayList<Language> _languages;

	private FSA _fsa; // The Finite State Automaton associated to the regular grammar

	private ArrayList<Run> _runs; // executions of the FSA on given words
	
	public Grammar() {
		_all_states = new SharedStates();
		_all_symbols = new SharedSymbols();
		_languages = new ArrayList<Language>();
		_runs = new ArrayList<Run>();
		_fsa = null;
	}

	// BUILDER

	public void set_name(String name) {
		_name = name;
	}

	// =TODO,FIXME= 
	public Language language(String name) {
		TRACING.fixme("Grammar.language: FIXME TEST");
		// /!\ à modifier pour avoir du sharing
		for(int i=0;i<this._languages.size();i++) {
			if(this._languages.get(i).name().equals(name))	//name().equals(name)) ==name)
				return this._languages.get(i);
		}
		Language language = new Language(this, name);		
		_languages.add(language);
		return language;
	}

	// =TODO=
	public void add_run(Run run) {
		_runs.add(run);
	}
	
	// =given= EXPORT
	
	// =TODO= 
	public String to_GRM() {
		TRACING.fixme("Grammar.to_GRM: TODO");
		// je pense que l'idée est de transformer la grammaire en une string ressemblant à ce qui se trouve dans les fichier de examples/grm
		String res ="";
		res += "Grammar("+this._name+"){\n";
		for(Language lang : this._languages) {
			res += "\t" + lang.name() + " ::= ";
			ArrayList<Production> prod = lang.productions();
			for(int i = 0; i < prod.size(); i++) {
				if(i == 0) {
					res += "\"" + prod.get(i).symbol.name() + "\""; 
						if(prod.get(i).language != null)
							res += ". " + prod.get(i).language.name();
				}
				else {
					res += "\n\t\t|" + " \"" + prod.get(i).symbol.name() + "\" ";
					if(prod.get(i).language != null)
						res += ". " + prod.get(i).language.name();
				}
			}
			res += "\n";
		}
		res += "}";
			
		return res;
	}

	// =given=
	public void as_FSA() {
		_fsa = new FSA(_all_states, _all_symbols, _name);
		for (Language language : _languages) {
			language.as_transition_of(_fsa);
		}
	}

	// =given=
	public void export() {
		// GRM 
		GRM.to_grm_file(OPTIONS.output_path, this._name, to_GRM());
		// FSA
		Language seed = null;
		if (!_languages.isEmpty())
			seed = _languages.get(0);
		this.as_FSA();
		TRACING.info("seed=" + seed.state().toString());
		this._fsa.initial_state(seed.state());
		this._fsa.export();
	}

	// =given= DEMO
	
	public void demo() {
		export();
		for (Run run : _runs)
			this._fsa.accept(run.initial_state, run.word);
	}

}
