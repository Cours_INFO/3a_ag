package grammar;

import finite_automata.FSA;
import finite_automata.State;
import finite_automata.Symbol;

public class Production {

	Symbol symbol;
	Language language;
	
	// CONSTRUCTOR
	
	public Production(Symbol symbol) {
		this.symbol = symbol;
		this.language = null;
	}
	
	public Production(Symbol symbol, Language language) {
		this.symbol = symbol;
		this.language = language;
	}
	
	 public Language language() {
		 return this.language;
	 }
	
	// EXPORT 
	
	// =TODO= 
	public void as_transition_from(FSA fsa, State source) {
		// plusieurs cas à considérer
		if (symbol.is_epsilon())
			source.is_accepting = true;
		else { 
			if (language == null)
				fsa.add_transition(source, symbol, fsa.exit_state());
			else //if language != null || language == source	
				fsa.add_transition(source, symbol, language.state());
		}
	}
	
}
