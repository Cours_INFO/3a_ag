package grammar;

import java.util.ArrayList;

import finite_automata.FSA;
import finite_automata.State;

public class Language {

	// L ::= production_1 | ... | production_n
	// name ::= the right hand side is list of productions

	private String _name;
	private ArrayList<Production> _productions;

	private State _state; // associated state in the finite automaton of the regular grammar

	public Language(Grammar grammar, String name) {
		_name = name;
		_productions = new ArrayList<Production>();
		_state = grammar.state(name);
	}

	// =given= BUILDER

	public void add_production(Production production) {
		_productions.add(production);
	}

	// =given= ACCESS TO PRIVATE FIELDS

	public String name() {
		return _name;
	}

	public State state() {
		return _state;
	}
	 public ArrayList<Production> productions(){
		 return this._productions;
	 }
	// =given= EXPORT

	public void as_transition_of(FSA fsa) {
		for (Production production : _productions) {
			production.as_transition_from(fsa, this._state);
		}
	}
}
