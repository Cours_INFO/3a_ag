/* Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / Polytech Grenoble, december 2021 */

package finite_automata;

public class Transition {

	public State source;
	public Symbol symbol;
	public State target;

	public Transition(State source, Symbol symbol, State target) {
		this.source = source;
		this.symbol = symbol;
		this.target = target;
	}
	
}
