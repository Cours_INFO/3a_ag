// TODO sur le modèle de SharedStates

package finite_automata;

import java.util.ArrayList;

import shared.TRACING;

public class SharedSymbols {

	// SHARING PRINCIPLE:
	// Already seen states are recorded in a list _deja_vu.
	// The methods state(name) and ith(int) give the SAME UNIQUE state object for
	// the same name or state id.

	public ArrayList<Symbol> set;

	public static final Symbol EPSILON = new Symbol("", 0);

	public SharedSymbols() {
		set = new ArrayList<Symbol>();
		set.add(EPSILON);
	}

	public int size() {
		return set.size();
	}

	// SHARING = FINDING THE UNIQUE REPRESENTATIVE

	public Symbol ith(int i) {
		return set.get(i);
	}

	// à modifier
	public Symbol symbol(String name) {
		for(int i=0;i<this.size();i++) {
			if(this.ith(i).name()==name)
				return this.ith(i);
		}
		TRACING.fixme("SharedSymbol.symbol: FIXME TEST");
		Symbol s = new Symbol(name, this.size());
		this.set.add(this.size(), s);
		return s;
	}

}
