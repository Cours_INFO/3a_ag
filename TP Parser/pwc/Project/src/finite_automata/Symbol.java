/* Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / Polytech Grenoble, december 2021 */

package finite_automata;

import shared.TRACING;

public class Symbol {

	private String _name;
	private int _id;

	public Symbol(String letter, int id) {
		_name = letter;
		_id = id;
		TRACING.trace("new Symbol(\"" + _name + "\", " + _id + ");");
	}

	public String name() {
		return _name;
	}

	public int id() {
		return _id;
	}

	public boolean is_epsilon() {
		return _name.isEmpty();
	}

	// EXPORT TO STRING

	public String toString() {
		return _id + " : " + _name ;
	}

}
