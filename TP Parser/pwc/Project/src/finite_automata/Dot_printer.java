/* Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / Polytech Grenoble, december 2021 */

package finite_automata;

import export.DOT;

public class Dot_printer {

	// OPTIONS

	static boolean SHOW_STATE_NUMBER = false;
	static boolean SHOW_STATE_NAME = true;

	// FIELDS

	private FSA _fsa;

	public Dot_printer(FSA fsa) {
		_fsa = fsa;
	}

	// STATES

	private String state_id(State state) {
		return String.format("%d", state.id());
	}

	private String state_decl(State state) {
		String label = new String();
		if (SHOW_STATE_NUMBER)
			label += state.id();
		if (SHOW_STATE_NUMBER && SHOW_STATE_NAME)
			label += ":";
		if (SHOW_STATE_NAME)
			label += state.name();
		return DOT.node_decl(state_id(state), label, "");
	}

	private String state_decls() {
		String state_decls = new String();

		state_decls += DOT.comment("ACCEPTING STATES");
		state_decls += DOT.node_style("shape=circle, peripheries=2, fontname=Georgia, fontsize=14, fontcolor=black");

		for (State state : _fsa.acceptings())
			state_decls += state_decl(state);

		state_decls += DOT.comment("UNREACHABLE STATES");
		state_decls += DOT.node_style("peripheries=0, style=filled, color=gray, fontcolor=white");

		for (State state : _fsa.unreachable())
			state_decls += state_decl(state);

		state_decls += DOT.comment("BLACK HOLE STATES");
		state_decls += DOT.node_style("peripheries=0, style=filled, color=black, fontcolor=white");

		for (State state : _fsa.holes())
			state_decls += state_decl(state);

		state_decls += DOT.comment("STANDARD (default) STATES");
		state_decls += DOT.node_style("peripheries=1, style=solid, color=black, fontcolor=black");

		for (State state : _fsa.standard())
			state_decls += state_decl(state);

		return state_decls;
	}

	// TRANSITIONS

	private String transition(State source, Symbol symbol, State target) {
		if (symbol == null)
			return initial_transition(target);
		else if (symbol.is_epsilon())
			return epsilon_transition(source, target);
		else
			return DOT.edge(state_id(source), symbol.name(), state_id(target), "");
	}

	private String initial_transition(State state) {
		return DOT.edge(this._fsa.name(), "", state_id(state), "color=blue");
	}

	private String epsilon_transition(State source, State target) {
		return DOT.edge(state_id(source), "&epsilon;", state_id(target), "color=gray, fontcolor=gray");
	}

	private String edge_decls() {
		String edge_decls = new String();
		edge_decls += DOT.comment("TRANSITIONS");
		edge_decls += DOT.edge_style("fontname=Courier");

		for (State state : _fsa.initials())
			edge_decls += initial_transition(state);

		for (Transition t : this._fsa.transitions())
			edge_decls += transition(t.source, t.symbol, t.target);

		return edge_decls;
	}

	// EXPORT

	public String toString() {
		String body = new String();
		body += "/* HORIZONTAL */ rankdir=LR;\n";
		body += DOT.comment("AUTOMATON NAME");
		body += DOT.node_decl(this._fsa.name(), null, "shape=plaintext, fontname=comic, fontsize=18, fontcolor=blue");
		body += state_decls();
		body += edge_decls();
		return "digraph " + this._fsa.name() + "{" + body + "\n}\n";
	}

}
