// TODO Deterministic Finite Automata as table of transitions

package finite_automata;

import export.HTML;

public class DFA {

	// SHARING

	private SharedStates _all_states;
	private SharedSymbols _all_symbols;

	public Symbol ith_symbol(int i) {
		return _all_symbols.ith(i);
	}

	public State ith_state(int i) {
		return _all_states.ith(i);
	}

	// FIELDS

	private String _name;

	private boolean _is_deterministic;

	private State[][] _table;
	private int _nb_symbols;
	private int _nb_states;

	private State _current_state; // for running the automaton

	public DFA(SharedStates all_states, SharedSymbols all_symbols, String name) {
		_all_states = all_states;
		_all_symbols = all_symbols;
		_name = name;
		_is_deterministic = true;
	}

	// ACCESS TO PRIVATE FIELDS

	public boolean is_determinitic() {
		return _is_deterministic;
	}

	// =GIVEN= INITIALIZE

	public void initialize() {
		_nb_states = _all_states.size();
		_nb_symbols = _all_symbols.size();
		_table = new State[_nb_states][_nb_symbols];
		for (int state = 0; state < _nb_states; state++)
			for (int symbol = 0; symbol < _nb_symbols; symbol++)
				_table[state][symbol] = SharedStates.HOLE;
	}

	// =TODO= ADD TRANSITION

	public void add(Transition t) {
		_table[t.source.id()][t.symbol.id()] = t.target;
	}

	public void add(State source, Symbol symbol, State target) {
		_table[source.id()][symbol.id()] = target;
	}

	// =TODO= DETERMINISTIC AUTOMATA ENGINE (only 15 lines of code)

	public void step(String letter) { // 1 line
		_current_state = _table[_current_state.id()][_all_symbols.symbol(letter).id()];
	}

	public boolean accept(State initial_state, String word) {
		_current_state = initial_state;
		for (int i = 0; i < word.length(); i++) {
			this.step(word.substring(i, i+1));
			if (!_current_state.has_outgoing_edges) {
				if (_current_state.is_accepting)
					return true;
				return false;
			}
		}
		if (_current_state.is_accepting)
			return true;
		return false;
	}

	// EXPORT

	// =given= export to HTML

	public String as_table() {
		// first row = all states
		String rows = new String();
		String row = HTML.cell(_name, "align=center");
		for (int state_id = 0; state_id < _nb_states; state_id++) {
			State source_state = ith_state(state_id);
			row += HTML.cell(source_state.toString(), "");
		}
		rows += HTML.row(row, "bgcolor=lightgray");
		// others rows = table
		for (int symbol_id = 0; symbol_id < _nb_symbols; symbol_id++) {
			Symbol symbol = _all_symbols.ith(symbol_id);
			// new row
			row = new String();
			// first column = all symbols
			row += HTML.cell(symbol.toString(), "bgcolor=lightgray");
			for (int state_id = 0; state_id < _nb_states; state_id++) {
				// others columns = targets
				State target_state = _table[state_id][symbol_id];
				if (target_state != null)
					row += HTML.cell(target_state.toString(), "");
				else
					row += HTML.cell("", "");
			}
			rows += HTML.row(row, "");
		}
		return HTML.table(rows, "border=1, cellspacing=1, cellpadding=2");
	}

	public void to_html() {
		HTML.to_html_file("", _name + "_DFA", this.as_table());
	}

}
