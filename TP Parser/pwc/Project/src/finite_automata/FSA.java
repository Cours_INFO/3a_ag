// TODO Finite State Automata

package finite_automata;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import shared.OPTIONS;
import shared.TRACING;

import export.AEF;
import export.DOT;

import finite_automata.Transition;
import finite_automata.Dot_printer;

public class FSA {

	// SHARING

	private SharedStates _all_states;
	private SharedSymbols _all_symbols;

	// CONSTANT

	public static final Symbol EPSILON = SharedSymbols.EPSILON;

	// FIELDS

	private String _name;

	// == automaton as a list of transitions
	private LinkedList<Transition> _transitions;
	
	// == automaton as transition table
	private DFA _dfa;

	// == runs
	List<Run> _runs;

	// == indexation of states
	private boolean _already_indexed;
	private ArrayList<State> _initials, _acceptings, _holes, _reachable, _unreachable, _standard;

	public FSA(SharedStates all_states, SharedSymbols all_symbols, String name) {
		_all_states = all_states;
		_all_symbols = all_symbols;
		
		_name = name;
		_transitions = new LinkedList<Transition>();
		
		_dfa = null;
		
		_runs = null;
		
		_already_indexed = false;
		_initials = new ArrayList<State>();
		_acceptings = new ArrayList<State>();
		_holes = new ArrayList<State>();
		_reachable = new ArrayList<State>();
		_unreachable = new ArrayList<State>();
		_standard = new ArrayList<State>();
	}

	// =given= SETTINGS
	
	public void initial_state(State state) {
		_initials.clear();
		_initials.add(state);
		_already_indexed = false;
	}
	
	// =given= ACCESS TO PRIVATE FIELDS

	public String name() {
		return _name;
	}
	
	public State exit_state() {
		return SharedStates.EXIT;
	}

	public LinkedList<Transition> transitions() {
		return _transitions;
	}

	public boolean is_determinitic() {
		return (_dfa != null && _dfa.is_determinitic());
	}

	public ArrayList<State> initials() {
		return _initials;
	}

	public ArrayList<State> acceptings() {
		if (!_already_indexed)
			index_states(_initials, _acceptings, _holes, _reachable, _unreachable, _standard);
		return _acceptings;
	}

	public ArrayList<State> holes() {
		if (!_already_indexed)
			index_states(_initials, _acceptings, _holes, _reachable, _unreachable, _standard);
		return _holes;
	}

	public ArrayList<State> reachable() {
		if (!_already_indexed)
			index_states(_initials, _acceptings, _holes, _reachable, _unreachable, _standard);
		return _reachable;
	}

	public ArrayList<State> unreachable() {
		if (!_already_indexed)
			index_states(_initials, _acceptings, _holes, _reachable, _unreachable, _standard);
		return _unreachable;
	}

	public ArrayList<State> standard() {
		if (!_already_indexed)
			index_states(_initials, _acceptings, _holes, _reachable, _unreachable, _standard);
		return _standard;
	}

	// BUILDER

	public void add_runs(Run run) {
		_runs.add(run);
	}

	// =TODO= INCREMENTAL ADDITON OF TRANSITION

	public void add_transition(State source, Symbol symbol, State target) {
		Transition tmp = new Transition(source,symbol,target);
		this._transitions.add(tmp);
	}

	// =TODO= CONSTRUCTION OF TRANSITION TABLES

	public DFA as_DFA() {
		DFA dfa = new DFA(_all_states, _all_symbols, _name);
		return dfa;
	}

	// =TODO= RUN

	public boolean accept(State initial_state, String word) {
		return false;
	}

	// =given= INDEXATION OF STATES as acceptings, reachable, unreachable, holes, ...

	// == OPERATIONS ON SET OF STATES

	// arguments: set_copy = computed, set2 = given & unchanged
	private void make_copy(ArrayList<State> set_copy, ArrayList<State> set) {
		set_copy.clear();
		for (State state : set) {
			set_copy.add(state);
		}
	}

	// arguments: set1 = given & modified, set2 = given & unchanged
	private void minus(ArrayList<State> set1, ArrayList<State> set2) {
		ArrayList<State> set1_copy = new ArrayList<State>();
		make_copy(set1_copy, set1); // copy for enumeration while removing elements
		for (State elt : set1_copy) {
			if (set2.contains(elt))
				set1.remove(elt);
		}
	}

	private String elements(ArrayList<State> set) {
		String elements = new String();
		Iterator<State> i = set.iterator();
		while (i.hasNext()) {
			State state = i.next();
			elements += state.toString();
			if (i.hasNext())
				elements += ", ";
		}
		return "{" + elements + "}";
	}

	// == REACHABILITY / CO-REACHABILITY

	// Reachability propagates from a state along the transitions of an automaton

	// arguments: state = given, set_of_states = computed
	private void accumulate_reachable_from(State state, ArrayList<State> set_of_states) {
		if (!set_of_states.contains(state)) {
			set_of_states.add(state);
			for (Transition t : this._transitions) {
				if (t.source == state)
					accumulate_reachable_from(t.target, set_of_states);
			}
		}
	}

	// Co-reachability propagates backward from a state along the reversed
	// transitions of an automaton

	// arguments: state = given, set_of_states = computed
	private void accumulate_coreachable_from(State state, ArrayList<State> set_of_states) {
		if (!set_of_states.contains(state)) {
			set_of_states.add(state);
			for (Transition t : this._transitions) {
				if (t.target == state)
					accumulate_coreachable_from(t.source, set_of_states);
			}
		}
	}

	// arguments: acceptings = computed
	private void compute_acceptings(ArrayList<State> acceptings) {
		acceptings.clear();
		for (State state : this._all_states) {
			if (state.is_accepting)
				acceptings.add(state);
		}
	}

	// arguments: initials = given, reachable = computed
	private void compute_reachable_from(ArrayList<State> initials, ArrayList<State> reachable) {
		reachable.clear();
		for (State state : initials) {
			accumulate_reachable_from(state, reachable);
		}
	}

	// arguments: acceptings = given, holes = computed
	private void compute_holes(ArrayList<State> acceptings, ArrayList<State> holes) {
		ArrayList<State> coreachable = new ArrayList<State>();
		for (State state : acceptings) {
			if (state.is_accepting)
				accumulate_coreachable_from(state, coreachable);
		}
		make_copy(holes, this._all_states.set);
		minus(holes, coreachable);
	}

	// arguments: initials = given, ... = computed
	public void index_states(ArrayList<State> initials, ArrayList<State> acceptings, ArrayList<State> holes,
			ArrayList<State> reachable, ArrayList<State> unreachable, ArrayList<State> standard) {
		// compute reachable from initials
		TRACING.info("FSA.classify_states:");
		TRACING.info("states = " + elements(this._all_states.set));
		TRACING.info("initials = " + elements(initials));
		compute_reachable_from(initials, reachable);
		TRACING.info("reachable = " + elements(reachable));
		// compute unreachable from reachable
		make_copy(unreachable, this._all_states.set);
		minus(unreachable, reachable);
		TRACING.info("unreachable = " + elements(unreachable));
		// compute acceptings
		compute_acceptings(acceptings);
		TRACING.info("acceptings = " + elements(acceptings));
		// compute holes from acceptings
		compute_holes(acceptings, holes);
		TRACING.info("holes = " + elements(holes));
		// compute standard = all_states - acceptings - unreachable - holes
		make_copy(standard, this._all_states.set);
		minus(standard, acceptings);
		minus(standard, unreachable);
		minus(standard, holes);
		TRACING.info("standard = " + elements(standard));
		_already_indexed = true;
	}

	// =given= EXPORT

	public void export() {
		this.to_dot_file();
		this.to_aef_file();
	}

	// =given= EXPORT TO AEF

	public String to_aef() {
		Aef_printer printer = new Aef_printer(this);
		return printer.toString();
	}

	public void to_aef_file() {
		AEF.to_aef_file(OPTIONS.output_path, this._name + "_FSA", to_aef());
	}

	// =given= EXPORT TO DOT

	public String to_dot() {
		Dot_printer printer = new Dot_printer(this);
		return printer.toString();
	}

	public void to_dot_file() {
		DOT.to_dot_file(OPTIONS.output_path, this._name + "_FSA", to_dot());
	}
}