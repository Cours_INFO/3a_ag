package aef;

import java.util.ArrayList;

import export.DOT;

public class Aef {

	String name;

	ArrayList<Transition> transitions;
	ArrayList<String> acceptings, initials;
	ArrayList<String> all_states;

	Aef() {
		initials = new ArrayList<String>();
		acceptings = new ArrayList<String>();
		all_states = new ArrayList<String>();
		transitions = new ArrayList<Transition>();
	}

	// BUILDER

	public void name(String name) {
		this.name = name;
	}

	public void record_state(String state) {
		if (!all_states.contains(state))
			all_states.add(state);
	}

	public void add_initial_state(String state) {
		if (!initials.contains(state))
			initials.add(state);
	}

	public void add_accepting_state(String state) {
		if (!acceptings.contains(state))
			acceptings.add(state);
	}

	public void add_transition(Transition transition) {
		transitions.add(transition);
	}

	// OPERATIONS ON SET OF STATES

	private void make_copy(ArrayList<String> set_copy, ArrayList<String> set) {
		for (String state : set) {
			set_copy.add(state);
		}
	}

	private void minus(ArrayList<String> set1, ArrayList<String> set2) {
		ArrayList<String> set1_copy = new ArrayList<String>();
		make_copy(set1_copy, set1); // copy for enumeration while removing elements
		for (String elt : set1_copy) {
			if (set2.contains(elt))
				set1.remove(elt);
		}
	}

	// REACHABILITY / CO-REACHABILITY

	// Reachability propagates from a state along the transitions of an automaton

	private void accumulate_reachable_from(String state, ArrayList<String> set_of_states) {
		if (!set_of_states.contains(state)) {
			set_of_states.add(state);
			for (Transition t : this.transitions) {
				if (t.source.equals(state))
					accumulate_reachable_from(t.target, set_of_states);
			}
		}
	}

	// Co-reachability propagates backward from a state along the reversed
	// transitions of an automaton

	private void accumulate_coreachable_from(String state, ArrayList<String> set_of_states) {
		if (!set_of_states.contains(state)) {
			set_of_states.add(state);
			for (Transition t : this.transitions) {
				if (t.target.equals(state))
					accumulate_coreachable_from(t.source, set_of_states);
			}
		}
	}

	// CLASSIFICATION OF STATES

	private void compute_reachable(ArrayList<String> reachable) {
		for (String i : this.initials) {
			accumulate_reachable_from(i, reachable);
		}
	}

	private void compute_holes(ArrayList<String> holes) {
		ArrayList<String> coreachable = new ArrayList<String>();
		for (String a : this.acceptings) {
			accumulate_coreachable_from(a, coreachable);
		}
		make_copy(holes, this.all_states);
		minus(holes, coreachable);
	}

	public void classify_states(ArrayList<String> holes, ArrayList<String> reachable, ArrayList<String> unreachable) {
		compute_holes(holes);
		compute_reachable(reachable);
		make_copy(unreachable, this.all_states);
		minus(unreachable, reachable);
	}

	// EXPORT

	// == export to DOT
	
	public String toDot() {

		ArrayList<String> holes = new ArrayList<String>();
		ArrayList<String> reachable = new ArrayList<String>();
		ArrayList<String> unreachable = new ArrayList<String>();

		classify_states(holes, reachable, unreachable);
	
		String dot_body = new String();

		dot_body += DOT.comment("AUTOMATON NAME");
		dot_body += DOT.node_decl(name, null, "shape=plaintext, fontname=comic, fontsize=18, fontcolor=blue");

		dot_body += DOT.comment("ACCEPTING STATES");
		dot_body += DOT.node_style("shape=circle, peripheries=2, fontname=Georgia, fontsize=14, fontcolor=black");

		for (String state : this.acceptings)
			dot_body += DOT.node_decl(state, null, "");

		dot_body += DOT.comment("UNREACHABLE STATES");
		dot_body += DOT.node_style("peripheries=0, style=filled, color=gray, fontcolor=white");

		for (String state : unreachable)
			dot_body += DOT.node_decl(state, null, "");

		dot_body += DOT.comment("BLACK HOLE STATES");
		dot_body += DOT.node_style("peripheries=0, style=filled, color=black, fontcolor=white");

		for (String state : holes)
			dot_body += DOT.node_decl(state, null, "");

		dot_body += DOT.comment("STANDARD (default) STATES");
		dot_body += DOT.node_style("peripheries=1, style=solid, color=black, fontcolor=black");

		ArrayList<String> standard = new ArrayList<String>();
		make_copy(standard, this.all_states);
		minus(standard, this.acceptings);
		minus(standard, unreachable);
		minus(standard, holes);
		for (String state : standard)
			dot_body += DOT.node_decl(state, null, "");

		dot_body += DOT.comment("TRANSITIONS");
		dot_body += DOT.edge_style("fontname=Courier");

		for (String state : this.initials)
			dot_body += new Transition(name, null, state).toDot();

		for (Transition t : transitions)
			dot_body += t.toDot();

		return DOT.dot("aef", dot_body);
	}

	public void toDotFile(String path, String file_name) {
		DOT.to_dot_file(path, file_name, this.toDot());
	}

	// == export to AEF

	public String toAef() {
		Aef_printer printer = new Aef_printer(this.acceptings);
		String aef_body = new String();
		
		for(String state : this.initials)
			aef_body += printer.initial_state(state);
		
		for(Transition t : this.transitions)
			aef_body += t.accept(printer);
		
		return printer.aef(name, aef_body);
	}

	public void toAefFile(String path) {
		Aef_printer.to_aef_file(path, "", this.toAef());
	}
}
